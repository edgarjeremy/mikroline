var route = [];
var selectedLatLng = null;
var readyOrder = false;
var bound = new google.maps.LatLngBounds();
// manado 1.476465, 124.863966
var map = new GMaps({
    div: '#map',
    lat: 1.476465,
    lng: 124.863966
});

map.addListener("dragend", function (e) {
    var lat = map.getCenter().lat();
    var lng = map.getCenter().lng();
    selectedLatLng = {lat: lat, lng: lng};
    bound.extend(map.getCenter());
    GMaps.geocode({
        lat: lat,
        lng: lng,
        callback: function (results, status) {
            if (status == 'OK' && readyOrder == false) {
                $("#loc-container input.current").val(results[0].formatted_address);
            }
        }
    });
});

function drawRoute(dari, ke) {
    map.drawRoute({
        origin: [dari.lat, dari.lng],
        destination: [ke.lat, ke.lng],
        travelMode: 'driving',
        strokeColor: '#131540',
        strokeOpacity: 0.6,
        strokeWeight: 6
    });
}