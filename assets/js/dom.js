$(function () {
    var $locContainer = $("#loc-container");
    var $controlContainer = $(".control");
    var $pointOverlay = $("#pointOverlay");
    var $confirmContainer = $("#confirm-container");
    var $loadingOverlay = $("#loading-overlay");
    var $driverOverlay = $("#driver-found");

    $(".from-container a").on("click", function () {
        $locContainer.show();
        $controlContainer.hide();
        $pointOverlay.show();
        $confirmContainer.show();
        $("#loc-container .from-input-container input").addClass("current").focus();
        return false;
    });

    $(".to-container a").on("click", function () {
        $locContainer.show();
        $controlContainer.hide();
        $pointOverlay.show();
        $confirmContainer.show();
        $("#loc-container .to-input-container input").addClass("current").focus();
        return false;
    });

    $("#confirm").on("click", function (e) {
        if (selectedLatLng && route.length != 2) {
            route.push(selectedLatLng);
            map.addMarker({
                lat: selectedLatLng.lat,
                lng: selectedLatLng.lng
            });
            var $next = $("#loc-container input:not(.current)");
            var $cur = $("#loc-container input.current");
            $cur.removeClass("current");
            $next.addClass("current");
            if(route.length > 1) {
                readyOrder = true;
                drawRoute(route[0], route[1]);
                $pointOverlay.hide();
                $("#confirm").text("ORDER SEKARANG (Rp.1.500)");
                map.fitBounds(bound);
            }
        }

        else if(readyOrder) {
            $loadingOverlay.fadeIn();
            setTimeout(function(){
                $driverOverlay.show();
            }, 3000);
        }

    });

});